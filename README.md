Overview
=====================
________________________________________________________________________________________________________________________

Spire.Office for .NET, incluidng Spire.Doc for .NET, Spire.XLS for .NET, Spire.PDF for .NET, Spire.DocViewer for .NET, Spire.PDFViewer for .NET and Spire.DataExport, is a composistive .NET Office library to manipulate Word, Excel, PDF document and export data without Microsoft Office and Adobe Acrobat. The environment it supports is .NET Framework from version 2.0 to 4.0 ClentProfile. Developers can create any ASP.NET and Winform applications in C# and Visual Basic with Spire.Office for .NET.

Functions:
====================
________________________________________________________________________________________________________________________

Word Processing

Spire.Office for .NET enables developers to fast generate, load, view, write, edit, save and print Word document. It can set text, paragraph and page ormatting after appending sections, paragraphs and text in document. Also, developers can insert image, hyperlink, table, watermark, bookmark, header/footer, comment, textbox, footnote/endnote, OLE object, barcode and edit these elements. What's more, developers can encrypt/decrypt ocument with password or protect Word with different protection types. 

Excel Processing

Spire.Office for .NET enables developers to create, load, write, edit and save Excel workbook. Developers can encrypt/decrypt workbook, lock worksheet, or protect with specified permissions. Also, developers can manipulate worksheet, including data, cell, row and column in this worksheet. What's more, developers can insert and edit variety of elements, such as chart, pivot table, image, background image, hyperlink, comments, formula, textbox, header/footer etc. 

PDF Processing 

Spire.Office for .NET enables developers to generate, load, view, draw, edit, save and print PDF document. It can merge/split PDF, set document properties, view preference and operate on pages, including page setup, page removing, pagination etc. It also support PDF security including encryption/decryption, digital signature. What's more, developers can draw, insert and edit object in PDF, including text, shape, comment, attachment, hyperlink, image, table, number list, formfield, watermark, header/footer etc. Among these object, text and image can be extracted.

Import/Exprot Data

Spire.Office for .NET enables developers to export data from Database, dataset, datatable and other datasource to Excel, Word, PDF and other popular document formats with fromatted data. Also, the data from Excel worksheet can be imported to datatable as well.

Conversion

Conversion is the featured function of Spire.Office for .NET. Developers can convert Word, Excel, CSV, RTF, Image, TEXT and HTML (including website) to PDF document with high fidelity. Also, other conversions between Word/Excel to other popular formats are suppported, such as Word to Image/HTML/RTF/TEXT/HTML/XML and Excel to HTML/XML/Image/CSV etc.

More inforamtion about Spire.Office for .NET: http://www.e-iceblue.com/Introduce/spire-office-for-net.html

Download Spire.Office for .NET: http://www.e-iceblue.com/Download/download-office-for-net-now.html
